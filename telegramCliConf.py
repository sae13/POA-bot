#!/usr/bin/env python3
# coding=utf-8
from pytg import Telegram
from random import randrange

#on pi
tg = Telegram(
 		telegram="/home/pi/App/telegram-cli/tg/bin/telegram-cli",
 		pubkey_file="/home/pi/App/telegram-cli/tg/tg-server.pub",
 	    port=randrange(8300,8999)
 	    )


#on fujitsu
#tg = Telegram(
	#	telegram="/home/saeb/App/telegram-cli/tg/bin/telegram-cli",
	#	pubkey_file="/home/saeb/App/telegram-cli/tg/bin/tg-server.pub",
	 #   port=8092
	  #  )
